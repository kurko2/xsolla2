#ifndef TORRENTATTRIBUTE_H
#define TORRENTATTRIBUTE_H

#include <QtGlobal>
#include <libtorrent/torrent_handle.hpp>

struct TorrentAttribute
{
    TorrentAttribute():
        valid(false),
        id(0),
        enable(false)
    {   }

    TorrentAttribute(const quint32 id, const QString & torrentName):
        valid(true),
        id(id),
        enable(true),
        torrentName(torrentName)
    {   }

    TorrentAttribute(const quint32 id, const QString & torrentName, const QString & fileName, const libtorrent::torrent_handle h):
        valid(true),
        id(id),
        enable(true),
        torrentName(torrentName),
        fileName(fileName),
        handle(h)
    {   }

    bool valid;
    quint32 id;
    bool enable;
    QString torrentName;
    QString fileName;
    libtorrent::torrent_handle handle;
};

#endif // TORRENTATTRIBUTE_H
