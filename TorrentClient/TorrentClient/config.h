#ifndef CONFIG_H
#define CONFIG_H

// ========= Макропределения и прочее =========

// Интервал переопроса списка torrent-файлов
#define REQUEST_INTERVAL                        60000//600000 // Период опроса web api c JSON списком (мс)
#define SAVE_RESUME_INTERVAL                    10000 // Период сохранения resume файлов (мс)

#define HTTP_ADDRESS_API                        "http://localhost/api/torrents/"

#define RESUME_FILE_SUFFIX                      ".resume" // Постфикс для имен resume файлов

#define SETTINGS_LANG_KEY                       "CurrentLanguage"


#endif // CONFIG_H
