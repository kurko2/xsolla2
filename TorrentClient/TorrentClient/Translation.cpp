#include "config.h"
#include "Translation.h"
#include <QDebug>

Translation::Translation()
{

}

Translation::Translation(QSettings *settings, QObject *parent) :
    QObject(parent),
    m_settings(settings)
{
    m_translator.reset(new QTranslator());
    m_currentLanguage = settings->value(SETTINGS_LANG_KEY, "en").toString();
    selectLanguage(m_currentLanguage);
}

Translation::~Translation()
{
}

void Translation::selectLanguage(const QString &language)
{
    if (language == "ru") {
        m_translator->load(QLocale(), ":/lang/qml_ru.qm");
        QGuiApplication::installTranslator(m_translator.data());
        qDebug() << "install ru";
    }

    if (language == "en") {
        QGuiApplication::removeTranslator(m_translator.data());
        qDebug() << "install en";
    }    

    m_currentLanguage = language;
    m_settings->setValue(SETTINGS_LANG_KEY, language);

    emit languageChanged();
}
