#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <QDir>
#include <QScopedPointer>
#include "Controller.h"
#include "TorrentListModel.h"
#include "Translation.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    // Настройки
    QSettings::setPath(QSettings::IniFormat, QSettings::SystemScope, QDir::currentPath());
    QScopedPointer<QSettings> settings(new QSettings(QSettings::IniFormat, QSettings::SystemScope, "xsolla", "TorrentClient"));

    // Языковая поддержка
    QScopedPointer<Translation> translation(new Translation(settings.data()));

    // Объект-контроллер
    QScopedPointer<Controller> controller(new Controller());

    // Объект-модель
    QScopedPointer<TorrentListModel> torrentListModel(new TorrentListModel(controller.data()));

    engine.rootContext()->setContextProperty("torrentListModel", torrentListModel.data());
    engine.rootContext()->setContextProperty("controller", controller.data());
    engine.rootContext()->setContextProperty("translation", translation.data());

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
