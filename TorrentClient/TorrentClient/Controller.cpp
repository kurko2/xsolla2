#include "Controller.h"

Controller::Controller(QObject *parent) :
    QObject(parent),
    torrentGetter(new TorrentGetter()), // Объект получения torrent-файлов
    torrentDownloader(new TorrentDownloader()) // Объект получения самих файлов
{
    torrentGetter->moveToThread(&getterThread);
    connect(&getterThread, &QThread::finished, torrentGetter, &TorrentGetter::deleteLater);
    connect(this, &Controller::runGetter, torrentGetter, &TorrentGetter::run, Qt::QueuedConnection);
    getterThread.start();

    torrentDownloader->moveToThread(&downloaderThread);
    connect(&downloaderThread, &QThread::finished, torrentDownloader, &TorrentDownloader::deleteLater);
    connect(this, &Controller::runDownloader, torrentDownloader, &TorrentDownloader::run, Qt::QueuedConnection);
    downloaderThread.start();

    // Добавление и удаление новыех и существующих
    connect(torrentGetter, &TorrentGetter::addTorrent, torrentDownloader, &TorrentDownloader::onAddTorrent, Qt::QueuedConnection);
    connect(this, &Controller::removeTorrent, torrentDownloader, &TorrentDownloader::onRemoveTorrent, Qt::QueuedConnection);

    // Enable/Disable
    connect(getTorrentGetter(), &TorrentGetter::stateTorrent, getTorrentDownloader(), &TorrentDownloader::onStateTorrent, Qt::QueuedConnection);

}

Controller::~Controller()
{
    getterThread.quit();
    getterThread.wait();

    torrentDownloader->setExit();
    downloaderThread.quit();
    downloaderThread.wait();
}

void Controller::start()
{
    // Запустить периодическую загрузку torrent-файлов
    emit runGetter();
    emit runDownloader();
}

void Controller::remove(const quint32 id)
{
    // Удалить файл, torrent и resume
    emit removeTorrent(id);
}
