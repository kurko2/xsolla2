#ifndef TRANSLATION_H
#define TRANSLATION_H

#include <QObject>
#include <QTranslator>
#include <QGuiApplication>
#include <QSettings>
#include <QPointer>


class Translation : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString emptyString READ getEmptyString NOTIFY languageChanged)

public:
    explicit Translation();
    explicit Translation(QSettings * m_settings, QObject *parent = nullptr);
    virtual ~Translation();

    QString getEmptyString() const {
        return QString();
    }

signals:
    void languageChanged();

public slots:
    void selectLanguage(const QString & language);

private:
    QSettings * m_settings;

    QScopedPointer<QTranslator> m_translator;

    QString m_currentLanguage;
};

#endif // TRANSLATION_H
