#ifndef TORRENTGETTER_H
#define TORRENTGETTER_H

#include <QObject>
#include <QNetworkReply>
#include <TorrentAttribute.h>
#include <QPointer>

class QTimer;
class QNetworkAccessManager;

/*
 * Задачей объекта этого класса будет периодическое (10 мин) получение списка torrent-файлов с сервера
 * И передача этой информации на обслуживание другому объекту-загрузчику файлов
 * Объет этого класса будет жить в потоке
 *
 */

class TorrentGetter : public QObject
{
    Q_OBJECT
public:
    explicit TorrentGetter(QObject *parent = nullptr);
    virtual ~TorrentGetter();

public slots:
    void run();

signals:
    void addTorrent(const quint32 id, const QString &torrentName, const QString &fileName);    
    void stateTorrent(const quint32 id, const bool value);

private:
    void getTorrents();
    void getTorrentFile(const quint32 id, const QString & name);

    QPointer<QNetworkAccessManager> m_netAccessManager;
    QPointer<QNetworkReply> m_torrentListReply;
    QHash<QNetworkReply*, TorrentAttribute> m_hashReply;

    const QString m_httpAddressAPI;

    QScopedPointer<QTimer> m_timer;

private slots:
    void onFinishedList();
    void onFinishedFile();
};

#endif // TORRENTGETTER_H
