#include "TorrentListModel.h"
#include "TorrentDownloader.h"

// Количество комплектов
TorrentListModel::TorrentListModel(Controller * controller)// :
{
    // Добавление и удаление новыех и существующих
    connect(controller->getTorrentGetter(), &TorrentGetter::addTorrent, this, &TorrentListModel::onAddTorrent);
    connect(controller, &Controller::removeTorrent, this, &TorrentListModel::onRemoveTorrent);

    // Движение progress идет от downloader-а
    connect(controller->getTorrentDownloader(), &TorrentDownloader::progressChanged, this, &TorrentListModel::onProgressChanged);

}

TorrentListModel::~TorrentListModel()
{
    m_rowsTorrent.clear();
    m_mapTorrent.clear();
}

int TorrentListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_mapTorrent.size();
}

QVariant TorrentListModel::data(const QModelIndex &index, int role) const
{
    if ((!index.isValid())
            || (index.row() >= m_rowsTorrent.size()))
        return QVariant();

    switch(role)
    {
    case FILENAME_ROLE:
        return m_rowsTorrent.at(index.row())->fileName;
    case TORRENTNAME_ROLE:
        return m_rowsTorrent.at(index.row())->torrentName;
    case PEERS_ROLE:
        return m_rowsTorrent.at(index.row())->peers;
    case SEEDS_ROLE:
        return m_rowsTorrent.at(index.row())->seeds;
    case PROGRESS_ROLE:
        return m_rowsTorrent.at(index.row())->progress;
    case ID_ROLE:
        return m_rowsTorrent.at(index.row())->id;
    default:
        return QVariant();
    }

    return QVariant();
}

QHash<int, QByteArray> TorrentListModel::roleNames() const
{
    const QHash<int, QByteArray> roles({{FILENAME_ROLE,"fileName"},{TORRENTNAME_ROLE,"torrentName"},{PEERS_ROLE,"peers"},{SEEDS_ROLE,"seeds"},{PROGRESS_ROLE,"progress"},{INFORMATION_ROLE,"information"},{ID_ROLE,"id"}});
    return roles;
}

void TorrentListModel::onAddTorrent(const quint32 id, const QString &torrentName, const QString &fileName)
{
    // Если таковой уже есть в списке, то ничего делать не нужно
    if (m_mapTorrent.contains(id))
        return;

    beginInsertRows(QModelIndex(), m_mapTorrent.size(), m_mapTorrent.size());
    QSharedPointer<TorrentSession> session = QSharedPointer<TorrentSession>(new TorrentSession(id, torrentName, fileName, 0.0));
    m_mapTorrent.insert(id, session);
    m_rowsTorrent.append(session);
    endInsertRows();
}

void TorrentListModel::onProgressChanged(const quint32 id, const qreal value, const qint32 peers, const qint32 seeds)
{
    QSharedPointer<TorrentSession> session = m_mapTorrent.value(id);
    if (session) {
        qint32 index = m_rowsTorrent.indexOf(session);
        session->progress = value;
        session->peers = peers;
        session->seeds = seeds;
        //dataChanged(createIndex(session->num,0), createIndex(session->num,0), {PROGRESS_ROLE, SEEDS_ROLE, PEERS_ROLE});
        dataChanged(createIndex(index,0), createIndex(index,0), {PROGRESS_ROLE, SEEDS_ROLE, PEERS_ROLE});

        //qDebug() << id << "-" << value;
    }
}

void TorrentListModel::onRemoveTorrent(const quint32 id)
{
    // Проверка на предмет несуществующего id
    QMap<quint32, QSharedPointer<TorrentSession> >::iterator i = m_mapTorrent.find(id);
    if (i == m_mapTorrent.end())
        return;

    qint32 index = m_rowsTorrent.indexOf(i.value());
    beginRemoveRows(QModelIndex(),index, index);

    m_rowsTorrent.removeAt(index);
    m_mapTorrent.take(i.key()).clear();

    endRemoveRows();
}
