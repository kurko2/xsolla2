#include <fstream>
#include <QCoreApplication>
#include <QTime>
#include <QDebug>
#include <QThread>
#include <QDebug>
#include <QFileInfo>
#include "TorrentDownloader.h"
#include "config.h"

namespace lt = libtorrent;

TorrentDownloader::TorrentDownloader(QObject *parent) :
    QObject(parent),
    m_exit(false)
{
    // Умышленно оставлен без переноса в поток, иначе просто не будет работать из-за while(1). Иначе вместо  sendPostedEvents(this) придется использовать  processEvents()
    m_timer.reset(new QTimer());
    connect(m_timer.data(), &QTimer::timeout, [=](){
        saveResume();
    });
    m_timer->start(SAVE_RESUME_INTERVAL);
}

TorrentDownloader::~TorrentDownloader()
{
}


void TorrentDownloader::run()
{
    lt::settings_pack pack;
    pack.set_int(lt::settings_pack::alert_mask, lt::alert::error_notification | lt::alert::storage_notification | lt::alert::status_notification);

    lt::session ses(pack);
    ltSession = &ses;

    lt::torrent_handle h;
    while (!m_exit.load()) {
        QCoreApplication::sendPostedEvents(this); // не работает для функций таймера

        std::vector<lt::alert*> alerts;
        ses.pop_alerts(&alerts);
        for (lt::alert const* a : alerts) {

            if (auto fa = lt::alert_cast<lt::torrent_finished_alert>(a)) {
                qDebug() << "finish " << QString::fromStdString(fa->handle.status().name);
                fa->handle.save_resume_data();
            }

            if (lt::alert_cast<lt::torrent_error_alert>(a)) {
                qDebug() << a->message().data();
                return;
            }

            if (auto rd = lt::alert_cast<lt::save_resume_data_alert>(a)) {
                std::ofstream of(rd->handle.status().name + RESUME_FILE_SUFFIX, std::ios_base::binary);
                of.unsetf(std::ios_base::skipws);
                lt::bencode(std::ostream_iterator<char>(of), *rd->resume_data);
            }


            if (auto st = lt::alert_cast<lt::state_update_alert>(a)) {
                for(quint32 i=0; i<st->status.size(); ++i) {
                    lt::torrent_status const& s = st->status[i];

                    //qDebug() << i << " " << s.handle.id() << "  " << QString::fromStdString( s.name ) << " Progress: " << s.progress;

                    TorrentAttribute attribute = m_hashFiles.value(s.handle.id());
                    if (attribute.valid)
                        emit progressChanged(attribute.id, s.progress, s.num_peers, s.num_seeds);
                }
            }
        }
        thread()->msleep(200);

        ses.post_torrent_updates();
    }
}

void TorrentDownloader::onAddTorrent(const quint32 id, const QString &torrentName, const QString &fileName)
{
    // Если таковой уже есть в списке, то ничего делать не нужно
    for(auto attribute: m_hashFiles)
        if (attribute.id == id)
            return;

    // Проверка на существование файла
    if (!QFileInfo::exists(torrentName))
        return;

    // Восстановление структуры файлы
    lt::add_torrent_params p;
    std::ifstream ifs(QString(fileName + RESUME_FILE_SUFFIX).toStdString(), std::ios_base::binary);
    ifs.unsetf(std::ios_base::skipws);
    p.resume_data.assign(std::istream_iterator<char>(ifs) , std::istream_iterator<char>());

    p.save_path = ".";
    p.url =  QString("file://").append(torrentName).toStdString();

    //ltSession->async_add_torrent(p);
    lt::torrent_handle handle = ltSession->add_torrent(p);
    handle.auto_managed(false);
    qDebug() << "add: " << handle.id();

    if (handle.is_valid())
        m_hashFiles.insert(handle.id(), TorrentAttribute(id, torrentName, fileName, handle));

}

void TorrentDownloader::onRemoveTorrent(const quint32 id)
{
    QHash<quint32, TorrentAttribute>::iterator i;
    for (i = m_hashFiles.begin(); i != m_hashFiles.end(); ++i)
        if (i.value().id == id) {
            // Удаление torrent файла и resume файла
            qDebug() << "torrent remove" << QFile::remove(i.value().torrentName);
            qDebug() << "resume remove:" << QFile::remove(i.value().fileName + RESUME_FILE_SUFFIX);

            qDebug() << "remove " << QString::fromStdString(i.value().handle.status().name);
            ltSession->remove_torrent(i.value().handle,lt::session_handle::delete_files);
            m_hashFiles.remove(i.key());

            return;
        }
}

// Проверка изменения состояния enable/disable
void TorrentDownloader::onStateTorrent(const quint32 id, const bool value)
{
    QHash<quint32, TorrentAttribute>::iterator i;
    for(i = m_hashFiles.begin(); i != m_hashFiles.end(); ++i) {
        if (i.value().id == id) {
            if (i.value().enable != value) {
                (value) ?
                        (i.value().handle.resume()) :
                        (i.value().handle.pause());

                i.value().enable = value;
                qDebug() << "enable/disable" << i.value().enable;
            }
        }
    }
}

// Периодическая поверка
void TorrentDownloader::saveResume()
{
    for(TorrentAttribute attribute: m_hashFiles) {
        attribute.handle.save_resume_data();
        qDebug() << "save " << QString::fromStdString(attribute.handle.status().name);
    }
}
