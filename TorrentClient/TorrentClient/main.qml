import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

ApplicationWindow {
    visible: true
    width: 700
    height: 480
    title: qsTr("Torrent client") + translation.emptyString

    ColumnLayout {
        anchors.fill: parent

        ListView {
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: torrentListModel
            delegate: TorrentItem {
                width: parent.width

                orderNumber: index
            }
        }

        RowLayout {
            Layout.fillWidth: true


            Button {
                Layout.margins: 5
                text: "Русский язык"
                onClicked: translation.selectLanguage("ru")
            }

            Button {
                Layout.margins: 5
                text: "English"
                onClicked: translation.selectLanguage("en")
            }

            Item {
                Layout.margins: 5
                Layout.fillWidth: true
            }

            Button {
                Layout.margins: 5
                text: qsTr("Exit") + translation.emptyString
                onClicked: {
                    Qt.quit()
                }
            }
        }
    }

    Component.onCompleted: {
        controller.start()
    }

}
