import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

GridLayout {
    columns: 5
    property int orderNumber


    Label {
        Layout.fillWidth: true
        Layout.margins: 10
        text: orderNumber
    }

//    TextField {
//        readOnly: true
//        //readOnly: true
//        clip: true
//        text: model.fileName
//    }
    Text {
        Layout.fillWidth: true
        Layout.minimumWidth: 200
        Layout.maximumWidth: 200

        //readOnly: true
        clip: true
        text: torrentName
    }

    Label {
        Layout.minimumWidth: 50
        Layout.maximumWidth: 50
        Layout.margins: 10
        clip: true
        text: peers + "/" + seeds
    }


    ProgressBar {
        Layout.margins: 5
        Layout.fillWidth: true
        width: 200
        value: progress
    }

    Button {
        Layout.minimumWidth: 70
        Layout.maximumWidth: 100
        Layout.margins: 5
        width: 100
        Layout.fillWidth: true;
        text: qsTr("Remove") + translation.emptyString
        onClicked: {
            controller.remove(id)
        }
    }
}
