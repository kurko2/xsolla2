#include "TorrentGetter.h"
#include "config.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QByteArray>
#include <QFile>
#include <libtorrent/torrent_info.hpp>
#include <QTimer>

TorrentGetter::TorrentGetter(QObject *parent) :
    QObject(parent),
    m_httpAddressAPI(HTTP_ADDRESS_API)
{
}

TorrentGetter::~TorrentGetter()
{
}

void TorrentGetter::run()
{
    m_netAccessManager = new QNetworkAccessManager(this);

    // Временное решение для механизма периодического опроса
    m_timer.reset(new QTimer());
    connect(m_timer.data(), &QTimer::timeout, [=](){
        getTorrents();
    });
    m_timer->start(REQUEST_INTERVAL);
    getTorrents();
}

void TorrentGetter::getTorrents()
{
    // Второй вариант
    m_torrentListReply = m_netAccessManager->get(QNetworkRequest(QUrl(m_httpAddressAPI)));
    connect(m_torrentListReply, &QNetworkReply::finished, this, &TorrentGetter::onFinishedList);
}

void TorrentGetter::getTorrentFile(const quint32 id, const QString & name)
{
    QString fileName = QString::number(id) + "/" + name + ".torrent";
    QNetworkReply * reply = m_netAccessManager->get(QNetworkRequest(QUrl(m_httpAddressAPI + fileName)));
    m_hashReply.insert(reply, TorrentAttribute(id, name));
    connect(reply, &QNetworkReply::finished, this, &TorrentGetter::onFinishedFile);
}

void TorrentGetter::onFinishedList()
{
    QString rawJson = m_torrentListReply->readAll();
    if (rawJson.size()) {

        QJsonParseError error;
        QJsonDocument jsonDoc = QJsonDocument::fromJson(rawJson.toUtf8(), &error);

        QJsonObject rootObj = jsonDoc.object();

        if (error.error != QJsonParseError::NoError) {
            //qCritical() << "config error: " << error.errorString() << " at "<< error.offset;
        }

        QJsonArray arrayTorrents = rootObj.value("list").toArray();
        for (const QJsonValue &torrentValue : arrayTorrents) {
            // Если torrent разрешен
            if (torrentValue.toObject().value("enable").toBool()) {

                // Загрузить сами файлы
                getTorrentFile(torrentValue.toObject().value("id").toInt(), torrentValue.toObject().value("name").toString());
                //qDebug() << "download: " << torrentValue.toObject().value("name").toString();
            }

            // Посылается даже когда файл еще не скачен
            emit stateTorrent(torrentValue.toObject().value("id").toInt(), torrentValue.toObject().value("enable").toBool());
        }
    }

    m_torrentListReply->deleteLater();
}

void TorrentGetter::onFinishedFile()
{
    QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());

    QByteArray rawFile = reply->readAll();
    if (rawFile.size()) {

        TorrentAttribute attribute = m_hashReply.value(reply);
        if (!attribute.valid)
            return;

        QString name = attribute.torrentName + ".torrent";
        QFile torrentFile(name);
        torrentFile.open(QIODevice::WriteOnly);
        torrentFile.write(rawFile);
        torrentFile.close();

        m_hashReply.remove(reply);

        libtorrent::torrent_info info(name.toStdString());
        emit addTorrent(attribute.id, name, QString::fromStdString(info.name()));
    }

    reply->deleteLater();
}
