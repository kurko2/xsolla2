#ifndef TORRENTDOWNLOADER_H
#define TORRENTDOWNLOADER_H

#include <QObject>
#include <QSharedPointer>
#include <cstdlib>
#include <libtorrent/session.hpp>
#include <libtorrent/torrent_info.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/bencode.hpp>
#include <libtorrent/torrent_status.hpp>
//#include <libtorrent/read_resume_data.hpp>
//#include <libtorrent/write_resume_data.hpp>
#include <libtorrent/error_code.hpp>
#include <libtorrent/magnet_uri.hpp>
#include "TorrentAttribute.h"
#include <QHash>
#include <QTimer>


/*
 * Задачей этого класса является непосредственный слив файлов через libtorrents
 */

class TorrentDownloader : public QObject
{
    Q_OBJECT
public:
    explicit TorrentDownloader(QObject *parent = nullptr);
    virtual ~TorrentDownloader();

    void setExit()
    {
        m_timer.clear();
        m_exit.store(true);
    }

signals:
    void progressChanged(const quint32 id, const qreal value, const qint32 peers, const qint32 seeds);

public slots:
    void run();

    void onAddTorrent(const quint32 id, const QString &torrentName, const QString &fileName);
    void onRemoveTorrent(const quint32 id);
    void onStateTorrent(const quint32 id, const bool value);

private:
    libtorrent::session * ltSession;

    QHash<quint32, TorrentAttribute>  m_hashFiles;

private:
    QScopedPointer<QTimer> m_timer;
    QAtomicInteger<bool> m_exit;
    void saveResume();
};

#endif // TORRENTDOWNLOADER_H
