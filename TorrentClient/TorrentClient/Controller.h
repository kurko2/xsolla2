#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QThread>
#include <QObject>
#include <QPointer>
#include "TorrentGetter.h"
#include "TorrentDownloader.h"

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = nullptr);
    virtual ~Controller();

    const TorrentGetter * getTorrentGetter()
    { return torrentGetter; }

    const TorrentDownloader * getTorrentDownloader()
    { return torrentDownloader; }


signals:
    void runGetter();
    void runDownloader();

    void removeTorrent(const quint32 id);

public slots:
    void start();
    void remove(const quint32 id);

private:
    QPointer<TorrentGetter> torrentGetter;
    QPointer<TorrentDownloader> torrentDownloader;

    QThread getterThread;
    QThread downloaderThread;

};

#endif // CONTROLLER_H
