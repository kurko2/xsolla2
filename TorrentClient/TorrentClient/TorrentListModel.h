#ifndef TORRENTLISTMODEL_H
#define TORRENTLISTMODEL_H

#include <QAbstractListModel>
#include "Controller.h"


struct TorrentSession
{
    TorrentSession(const quint32 id, const QString &torrentName, const QString &fileName, const qreal progress):
        id(id),
        torrentName(torrentName),
        fileName(fileName),
        progress(progress),
        peers(0),
        seeds(0)
    {   }

    quint32 id; // идентификатор из конфигуратора
    QString torrentName;
    QString fileName;
    qreal progress;
    qint32 peers;
    qint32 seeds;
};

class TorrentListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    TorrentListModel(Controller *controller);
    virtual ~TorrentListModel();

    // Обязаны быть переопределены
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

private:

    enum Column {
        HASH_NUMBER_ROLE,
        FILENAME_ROLE,
        TORRENTNAME_ROLE,
        PEERS_ROLE,
        SEEDS_ROLE,
        PROGRESS_ROLE,
        INFORMATION_ROLE,
        ID_ROLE,
        END_OF_COLUMN
    };

    QMap<quint32, QSharedPointer<TorrentSession> > m_mapTorrent; // упорядочено по id из конфигуратора
    QList<QSharedPointer<TorrentSession> > m_rowsTorrent; // упорядочено по порядковому номеру в списке

private slots:
    void onAddTorrent(const quint32 id, const QString & torrentName, const QString & fileName);
    void onProgressChanged(const quint32 id, const qreal value, const qint32 peers, const qint32 seeds);

    void onRemoveTorrent(const quint32 id);

};

#endif // TORRENTLISTMODEL_H
